# Redux

### Three laws of Redux

1.  [ ] TODO
2.  The only way to change the state is by sending a signal to the store.
3.  The state is immutable and cannot change in place.

### Store

- _reducer_ : as the first argument
- _state_ : Pass the initial state only when server side rendering, otherwise pass it through reducer

> State initialization condition: <br>
> => Booting up server rendered app from JSON state payload <br>
> => “Resuming” the app from a state saved into local storage

```
import { createStore } from redux
store = createStore(reducer, state)
```

### Reducer

- current _state_
- _action_

_always return something!_ if all cases fail, return original state <br>
_Reducer must be pure._ A pure function is one that returns the exact the same output given the same input.

```
reducer = function(state, action) {
	if(action.type === "ONACTION") {
		return action.payload
		}
		return state
	}
```

**OR**

```
switch (action.type) {
    case ADD_ARTICLE:
    	return { ...state, articles: [...state.articles, action.payload] };
    default:
    	return state;
}
```
<span style="color:red;">
Object spread operator has been used above. It's available in stage 3.
</span>

Reducer calculates the new state and returns a new object.

### Subscribe

listening on the state changes

```
store.subscribe(() => {
    console.log("changed store is: ", store.getState())
})
```

### Dispatch

dispatches an action

```
store.dispatch({type: "ONACTION", payload: ""})
```

#### Action

- _type property_ : Every action needs one for describing how the state should change
- _payload_
- **action creator** :every action should be wrapped in a new function
